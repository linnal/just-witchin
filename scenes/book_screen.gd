extends Node2D


signal go_to_table
signal go_to_garden

signal update

var PotionPage = preload("res://ui-items/potion_page.tscn")
var potion_page_node = null

func _ready():
    self.connect("update", update_book)
    _prepare_book_page()

func _process(delta):
    pass

func _input(event):
    if Input.is_key_pressed(KEY_SPACE):
        GameState.potion_preparation_succeeded()
        _prepare_book_page()

func _prepare_book_page():
    if potion_page_node :
        remove_child(potion_page_node)

    var potions_collection = GameState.potions_collection
    for index in potions_collection.size():
        if index == GameState.current_potion:
            potion_page_node = PotionPage.instantiate()
            potion_page_node.init(potions_collection[index])
            add_child(potion_page_node)

func update_book():
    _prepare_book_page()

func _on_table_button_pressed():
    go_to_table.emit()

func _on_garden_button_pressed():
    go_to_garden.emit()
