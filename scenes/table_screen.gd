extends Node2D

signal go_to_book
signal go_to_garden
signal cook

var BookIngredient = preload("res://ui-items/book_ingredient.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
    cook.connect(prepare_cooking)
    _update_label()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    pass

func prepare_cooking():
    _update_label()

func _on_book_button_pressed():
    go_to_book.emit()

func _on_garden_button_pressed():
    go_to_garden.emit()

func _update_label():
    var current_count = GameState.get_invetory_size()
    var required_count = GameState.get_current_ingredients_required_size()
    $PanelContainer/MarginContainer/InventoryCount.text = "%d/%d Ingredients" % [current_count, required_count]


func _on_cooking_pot_pressed():
    var tween = create_tween()
    tween.set_parallel(true)
    for ingredient in GameState.inventory:
        var ui_ingredient = BookIngredient.instantiate()
        ui_ingredient.init("", ingredient.icon)
        var follow = PathFollow2D.new()
        follow.add_child(ui_ingredient)
        $IngredientsPath2D.add_child(follow)
        tween.tween_property(follow, "progress_ratio", 1.0, 0.7)\
            .set_ease(Tween.EASE_IN)\
            .set_trans(Tween.TRANS_CUBIC)
        tween.tween_property(ui_ingredient, "scale", Vector2.ZERO, 0.7)\
            .set_ease(Tween.EASE_IN)\
            .set_trans(Tween.TRANS_EXPO)
        tween.chain()
        tween.tween_callback(splash_sound)
        tween.chain()

    await tween.finished

    $Timer.start()


func _is_win_case():
    AudioManager._play_audio("res://assets/Audio/SFX_SignSuccess.mp3")
    if GameState.get_invetory_size() != GameState.get_current_ingredients_required_size():
        return false

    var inventory_names = {}
    for inventory_ingredient in GameState.inventory:
        inventory_names[inventory_ingredient.name] = ""

    for potion_ingredient in GameState.get_current_potion().items:
        if !inventory_names.has(potion_ingredient.name):
            return false

    return true


func _on_timer_timeout():
    if _is_win_case():
        GameState.potion_preparation_succeeded()
        $CookingGreensmoke.show()
    else:
        GameState.clean()
        $CookingRedsmoke.show()
        $RetryTimer.start()

    $HiderTimer.start()


func splash_sound():
    $SplashSound.pitch_scale = randf_range(0.8, 1.2)
    $SplashSound.play()


func _on_retry_timer_timeout():
    AudioManager._play_audio("res://assets/Audio/SFX_SignFail.mp3")
    get_tree().change_scene_to_file("res://scenes/retry.tscn")



func _on_hider_timer_timeout():
    $CookingGreensmoke.hide()
    $CookingRedsmoke.hide()
