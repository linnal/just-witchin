extends Node2D

signal go_to_book
signal go_to_table
signal activate_lock

var GardenPlant = preload("res://ui-items/garden_plant.tscn")

@onready var spawners = $SpawnPoints.get_children()

@onready var current_potion: Potion = GameState.get_current_potion()

func _ready():
    self.connect("activate_lock", signal_activate_lock)
#    populate_garden()
    _update_inventory_label(GameState.inventory.size(), current_potion.items.size())

func _on_book_button_pressed():
    go_to_book.emit()

func _on_table_button_pressed():
    go_to_table.emit()

func signal_activate_lock():
    _update_inventory_label(GameState.inventory.size(), current_potion.items.size())
    current_potion = GameState.get_current_potion()
    for child in $Plants.get_children():
        child.queue_free()

    populate_garden()
    if GameState.inventory.size() == 0:
        hide_button($BookButton)
        hide_button($TableButton)

func deactivate_lock(button):
    var t = create_tween()
    t.tween_property(button, "scale", Vector2.ONE, 0.45)\
    .set_ease(Tween.EASE_IN)\
    .set_trans(Tween.TRANS_BACK)

func hide_button(button):
    var t = create_tween()
    t.tween_property(button, "scale", Vector2.ZERO, 0.45)\
    .set_ease(Tween.EASE_IN)\
    .set_trans(Tween.TRANS_BACK)

# TODO make sure to have always present current potions ingredients
func populate_garden():
    var ingredients = GameState.ingredients_collection
    var plants = []

    for spawner in spawners:
        var random_ingredient_pos = randi() % ingredients.size()
        var ingredient: Ingredient = ingredients[random_ingredient_pos]

        var garden_plant = GardenPlant.instantiate()
        garden_plant.init(spawner.global_position, ingredient)
        garden_plant.collect_plant.connect(add_plant_to_inventory)

        plants.append(garden_plant)

    for i in range(0, current_potion.items.size()):
        var garden_plant = GardenPlant.instantiate()
        var spawner_pos = spawners[i].global_position
        var ingredient =  current_potion.items[i]
        garden_plant.init(spawner_pos, ingredient)
        garden_plant.collect_plant.connect(add_plant_to_inventory)

        plants[i] = garden_plant

    plants.shuffle()
    for gp in plants:
        $Plants.add_child(gp)



func add_plant_to_inventory(resource):
    AudioManager._play_audio("res://assets/Audio/SFX_PickingIngredient.mp3")
    var count_ingredients_required = current_potion.items.size()
    var count_ingredients_inventory = GameState.inventory.size()

    if count_ingredients_inventory < count_ingredients_required:
        GameState.add_to_inventory(resource)
        count_ingredients_inventory += 1
        _update_inventory_label(count_ingredients_inventory, count_ingredients_required)

    if count_ingredients_inventory == count_ingredients_required:
        deactivate_lock($BookButton)
        deactivate_lock($TableButton)


func _update_inventory_label(current, required):
    $PanelContainer/MarginContainer/InventoryCount.text = "%d/%d Ingredients" % [current, required]
