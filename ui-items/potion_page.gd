extends Node2D

var BookIngredient = preload("res://ui-items/book_ingredient.tscn")

func init(potion):
        $Icon.texture = potion.icon
        $Name.text = potion.name
        $Description.text = potion.description

        var ingredients = potion.items
        for ingredient in ingredients:
            var book_ingredient = BookIngredient.instantiate()
            book_ingredient.init(ingredient.name, ingredient.icon)
            $Control/GridContainer.add_child(book_ingredient)
