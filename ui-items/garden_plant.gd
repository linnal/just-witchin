extends Node2D

signal collect_plant(resource)

var ingredient_resource: Ingredient

func init(pos, resource):
    position = pos
    ingredient_resource = resource
    $Background.texture = ingredient_resource.plantImage
    $Sprite.texture = ingredient_resource.plantImage


func _on_area_2d_mouse_entered():
    $Background.show()


func _on_area_2d_mouse_exited():
    $Background.hide()


func _on_area_2d_input_event(viewport, event, shape_idx):
    if event is InputEventMouseButton && event.is_pressed():
        collect_plant.emit(ingredient_resource)
        queue_free()
