extends Resource

class_name Ingredient

@export var name: String = ""
@export var icon: Texture
@export var plantImage: Texture
