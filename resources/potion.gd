extends Resource

class_name Potion

@export var name: String = ""
@export var description: String = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
@export var icon: Texture
@export var items: Array[Resource]
@export var status: String = "pending"
