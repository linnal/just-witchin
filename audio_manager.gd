extends Node

var audio_player

# Called when the node enters the scene tree for the first time.
func _ready():
	audio_player = get_node("/root/BgMusic").get_child(0)

func _play_audio(soundpath):
	if !audio_player.playing:
		audio_player.pitch_scale = randf_range(0.8, 1.2)
		audio_player.stream = load(soundpath)
		audio_player.play()
