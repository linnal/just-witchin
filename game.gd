extends Node2D

const SCREEN_SIZE  = 3
var current_screen = 0

@onready var book_screen: Node2D = $BookScreen
@onready var table_screen: Node2D = $TableScreen
@onready var garden_screen: Node2D = $GardenScreen
@onready var screens = [book_screen, table_screen, garden_screen]

func _ready():
    _go_to_book()

func _go_to_table():
    AudioManager._play_audio("res://assets/Audio/SFX_TransitionCauldron.mp3")
    book_screen.hide()
    garden_screen.hide()
    table_screen.show()

    table_screen.emit_signal("cook")


func _go_to_book():
    AudioManager._play_audio("res://assets/Audio/SFX_TransitionBook.mp3")
    book_screen.show()
    garden_screen.hide()
    table_screen.hide()

    book_screen.emit_signal("update")

func _go_to_garden():
    AudioManager._play_audio("res://assets/Audio/SFX_TransitionGarden.mp3")
    book_screen.hide()
    garden_screen.show()
    table_screen.hide()

    garden_screen.emit_signal("activate_lock")
