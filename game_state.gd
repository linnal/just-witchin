extends Node

var potions_collection = preload("res://resources/PotionsCollection.tres").items
var current_potion = 0

var ingredients_collection = preload("res://resources/IngredientsCollection.tres").items

var inventory = []

func potion_preparation_succeeded():
    potions_collection[current_potion].status = "success"
    current_potion = (current_potion + 1) % potions_collection.size()
    inventory = []

func clean():
    inventory = []

func get_current_potion():
    return potions_collection[current_potion]

func add_to_inventory(ingredient):
    inventory.append(ingredient)

func get_invetory_size():
    return inventory.size()

func get_current_ingredients_required_size():
    return potions_collection[current_potion].items.size()
